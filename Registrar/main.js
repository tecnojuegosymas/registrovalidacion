const formulario = document.getElementById('formulario');
const inputs = document.querySelectorAll('#formulario input');

const expresiones = {
	nombre: /^[a-zA-ZÀ-ÿ\s]{3,40}$/, // Letras y espacios, pueden llevar acentos.
    apellido: /^[a-zA-ZÀ-ÿ\s]{5,40}$/, // Letras y espacios, pueden llevar acentos.
    telefono: /^\d{7,14}$/, // 7 a 14 numeros.
	contrasena: /^.{4,12}$/, // 4 a 12 digitos.
	correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
	
}
    const Validar ={
        nombre: false,
        apellido: false,
        telefono: false,
        contrasena: false,
        correo: false,
    }

const validarForm =(e) =>{
    switch (e.target.name){
        case "nombre":
            if(expresiones.nombre.test(e.target.value)){
                document.getElementById('Grupo2N').classList.remove('Formulario2G-incorrecto');
                document.getElementById('Grupo2N').classList.add('Formulario2G-correcto');
                document.querySelector('#Grupo2N i').classList.add('fa-check-circle');
                document.querySelector('#Grupo2N i').classList.remove('fa-times-circle');
                document.querySelector('#Grupo2N .Formulario2I1E').classList.remove('Formulario2I1E-activo');
                Validar['nombre']=true;
        
        
            } else{
                document.getElementById('Grupo2N').classList.add('Formulario2G-incorrecto');
                document.getElementById('Grupo2N').classList.remove('Formulario2G-correcto');
                document.querySelector('#Grupo2N i').classList.add('fa-times-circle');
                document.querySelector('#Grupo2N i').classList.remove('fa-check-circle');
                document.querySelector('#Grupo2N .Formulario2I1E').classList.add('Formulario2I1E-activo');
                Validar['nombre']=false;
   
        
            }
        break;
        case "apellido":
            if(expresiones.apellido.test(e.target.value)){
                document.getElementById('Grupo2A').classList.remove('Formulario2G-incorrecto');
                document.getElementById('Grupo2A').classList.add('Formulario2G-correcto');
                document.querySelector('#Grupo2A i').classList.add('fa-check-circle');
                document.querySelector('#Grupo2A i').classList.remove('fa-times-circle');
                document.querySelector('#Grupo2A .Formulario2I1E').classList.remove('Formulario2I1E-activo');
                Validar['apellido']=true;
        
        
            } else{
                document.getElementById('Grupo2A').classList.add('Formulario2G-incorrecto');
                document.getElementById('Grupo2A').classList.remove('Formulario2G-correcto');
                document.querySelector('#Grupo2A i').classList.add('fa-times-circle');
                document.querySelector('#Grupo2A i').classList.remove('fa-check-circle');
                document.querySelector('#Grupo2A .Formulario2I1E').classList.add('Formulario2I1E-activo');
                Validar['apellido']=false;
    
        
            }
    
        break;
        case "telefono":
            if(expresiones.telefono.test(e.target.value)){
                document.getElementById('Grupo2T').classList.remove('Formulario2G-incorrecto');
                document.getElementById('Grupo2T').classList.add('Formulario2G-correcto');
                document.querySelector('#Grupo2T i').classList.add('fa-check-circle');
                document.querySelector('#Grupo2T i').classList.remove('fa-times-circle');
                document.querySelector('#Grupo2T .Formulario2I1E').classList.remove('Formulario2I1E-activo');
                Validar['telefono']=true;

        
        
            } else{
                document.getElementById('Grupo2T').classList.add('Formulario2G-incorrecto');
                document.getElementById('Grupo2T').classList.remove('Formulario2G-correcto');
                document.querySelector('#Grupo2T i').classList.add('fa-times-circle');
                document.querySelector('#Grupo2T i').classList.remove('fa-check-circle');
                document.querySelector('#Grupo2T .Formulario2I1E').classList.add('Formulario2I1E-activo');
                Validar['telefono']=false;

        
            }
    
        break;
        case "contrasena":
            if(expresiones.contrasena.test(e.target.value)){
                document.getElementById('Grupo2P').classList.remove('Formulario2G-incorrecto');
                document.getElementById('Grupo2P').classList.add('Formulario2G-correcto');
                document.querySelector('#Grupo2P i').classList.add('fa-check-circle');
                document.querySelector('#Grupo2P i').classList.remove('fa-times-circle');
                document.querySelector('#Grupo2P .Formulario2I1E').classList.remove('Formulario2I1E-activo');
        
        
            } else{
                document.getElementById('Grupo2P').classList.add('Formulario2G-incorrecto');
                document.getElementById('Grupo2P').classList.remove('Formulario2G-correcto');
                document.querySelector('#Grupo2P i').classList.add('fa-times-circle');
                document.querySelector('#Grupo2P i').classList.remove('fa-check-circle');
                document.querySelector('#Grupo2P .Formulario2I1E').classList.add('Formulario2I1E-activo');
        
            }
            validarContrsena2();
    
        break;
        case "contrasena2":
            validarContrsena2();

    
        break;
        case "correo":
            if(expresiones.correo.test(e.target.value)){
                document.getElementById('Grupo2C').classList.remove('Formulario2G-incorrecto');
                document.getElementById('Grupo2C').classList.add('Formulario2G-correcto');
                document.querySelector('#Grupo2C i').classList.add('fa-check-circle');
                document.querySelector('#Grupo2C i').classList.remove('fa-times-circle');
                document.querySelector('#Grupo2C .Formulario2I1E').classList.remove('Formulario2I1E-activo');
                Validar['correo']=true;
        
        
            } else{
                document.getElementById('Grupo2C').classList.add('Formulario2G-incorrecto');
                document.getElementById('Grupo2C').classList.remove('Formulario2G-correcto');
                document.querySelector('#Grupo2C i').classList.add('fa-times-circle');
                document.querySelector('#Grupo2C i').classList.remove('fa-check-circle');
                document.querySelector('#Grupo2C .Formulario2I1E').classList.add('Formulario2I1E-activo');
                Validar['correo']=false;    
        
            }
    
        break;

    }
}

const validarContrsena2=()=>{
    const inputContrasena1 = document.getElementById('contrasena');
    const inputContrasena2 = document.getElementById('contrasena2');
    if(inputContrasena1.value !== inputContrasena2.value){
                document.getElementById('Grupo2P2').classList.add('Formulario2G-incorrecto');
                document.getElementById('Grupo2P2').classList.remove('Formulario2G-correcto');
                document.querySelector('#Grupo2P2 i').classList.add('fa-times-circle');
                document.querySelector('#Grupo2P2 i').classList.remove('fa-check-circle');
                document.querySelector('#Grupo2P2 .Formulario2I1E').classList.add('Formulario2I1E-activo');
                Validar['contrasena']=false;
    }else{
        document.getElementById('Grupo2P2').classList.remove('Formulario2G-incorrecto');
                document.getElementById('Grupo2P2').classList.add('Formulario2G-correcto');
                document.querySelector('#Grupo2P2 i').classList.remove('fa-times-circle');
                document.querySelector('#Grupo2P2 i').classList.add('fa-check-circle');
                document.querySelector('#Grupo2P2 .Formulario2I1E').classList.remove('Formulario2I1E-activo');
                Validar['contrasena']=true;
    }



}
inputs.forEach((input) =>{
    input.addEventListener('keyup', validarForm);
    input.addEventListener('blur', validarForm);
});
formulario.addEventListener('submit', (e) => {
    e.preventDefault();
    const condiciones= document.getElementById('condiciones');
    if(Validar.nombre && Validar.apellido && Validar.telefono && Validar.contrasena && Validar.correo && condiciones.checked){
        formulario.reset();
        document.getElementById('Fomulario2M1E').classList.add('Fomulario2M1E-activo');
        setTimeout(()=>{
            document.getElementById('Fomulario2M1E').classList.remove('Fomulario2M1E-activo');
        },4000);
        document.querySelectorAll('.Formulario2G-correcto').forEach((icono)=>{
            icono.classList.remove('Formulario2G-correcto');

        });
    }else{
        document.getElementById('Formulario2M').classList.add('Formulario2M-activo');
        setTimeout(()=>{
            document.getElementById('Formulario2M').classList.remove('Formulario2M-activo');
        },4000);
    }
});